import pandas as pd

from trade_alert.Utils.talib_utils import all_futures_pairs, write_to_file, get_candle_data, \
    get_hull_moving_average, get_ma, get_avg_tr, get_candle_data_with_timestamp, get_all_coin_pairs
from trade_alert.Utils.str_utils import rsi_identifier, volume_monitor, growth_monitor, ath_diff, \
    stochastic_crossover
from datetime import datetime, timedelta
import talib
from trade_alert.Utils.decorators import timeit


@timeit
def rsi_below_20():
    required_coins = list()
    for coin_pair in get_all_coin_pairs():
        try:
            current_rsi = rsi_identifier(coin_pair, '5m')
            if float(current_rsi) >= 50:
                required_coins.append([coin_pair, current_rsi])
                print(coin_pair, current_rsi)
        except Exception as e:
            print(e, coin_pair)
            continue
    write_to_file([str(i) + "\n" for i in required_coins]) if required_coins else print("no coins in given scenario")


def price_below_200sma():
    result = [['coin', 'ma_200', 'trend', 'diff%']]
    for coin_pair in get_all_coin_pairs():
        df = get_candle_data('binance', coin_pair, '1d', 202, 'Futures')
        ma_200 = get_ma(df, 200)
        trend = ''
        try:
            if df['high'][len(df) - 1] > ma_200[1][len(df) - 1] > df['close'][len(df) - 1]:
                trend = 'trying to cross'
            elif ma_200[1][len(df) - 1] > df['close'][len(df) - 1]:
                trend = 'below'
            elif ma_200[1][len(df) - 1] < df['close'][len(df) - 1]:
                trend = 'above'
            diff = (ma_200[1][len(df) - 1] - df['close'][len(df) - 1]) / df['close'][len(df) - 1]
            result.append([coin_pair, ma_200[1][len(df) - 1], trend, diff])
        except Exception as e:
            print("exception", e)
            continue
    write_to_file([str(i) + "\n" for i in result])


@timeit
def str_3_volume_hike(*coin):
    required_coins = list()
    coins_to_fetch = coin if coin else all_futures_pairs
    for coin_pair in coins_to_fetch:
        try:
            volume_increased = volume_monitor(coin_pair, '15m')
            if volume_increased:
                required_coins.append({coin_pair: volume_increased})
        except Exception as e:
            print(e, coin_pair)
            continue
    if required_coins:
        sorted_coins = sorted(required_coins,
                              key=lambda x: datetime.strptime(list(x.values())[0][0]['time'], "%Y-%m-%d %H:%M:%S"),
                              reverse=True)
        for i in sorted_coins:
            print(i)
        write_to_file([str(i) + "\n" for i in sorted_coins])
    else:
        print("no coins in given scenario.")


@timeit
def coin_with_high_growth():
    required_coins = list()
    for coin_pair in all_futures_pairs:
        change_in_price = growth_monitor(coin_pair)
        # if change_in_price > 100 or change_in_price < -100:
        required_coins.append({'coin': coin_pair, 'price_change': change_in_price})
    if required_coins:
        sorted_coins = sorted(required_coins, key=lambda i: i['price_change'], reverse=True)
        for coin in sorted_coins:
            print(coin)
        write_to_file([str(i) + "\n" for i in sorted_coins])
    else:
        print("no coins in given scenario..")


@timeit
def get_ath_diff():
    required_coins = list()
    for coin_pair in get_all_coin_pairs():
        change_in_price = ath_diff(coin_pair)
        # if change_in_price > 100 or change_in_price < -100:
        required_coins.append({'coin': coin_pair, 'price_change': change_in_price})
    if required_coins:
        sorted_coins = sorted(required_coins, key=lambda i: i['price_change']['diff_to_previous_high'], reverse=True)
        for coin in sorted_coins:
            print(coin)
        write_to_file([str(i) + "\n" for i in sorted_coins])
    else:
        print("no coins in given scenario...")

def get_ath_diff_async():
    required_coins = list()
    for coin_pair in get_all_coin_pairs():
        change_in_price = ath_diff(coin_pair)
        # if change_in_price > 100 or change_in_price < -100:
        required_coins.append({'coin': coin_pair, 'price_change': change_in_price})
    if required_coins:
        sorted_coins = sorted(required_coins, key=lambda i: i['price_change']['diff_to_previous_high'], reverse=True)
        for coin in sorted_coins:
            print(coin)
        write_to_file([str(i) + "\n" for i in sorted_coins])
    else:
        print("no coins in given scenario...")

@timeit
def get_hma_crossover():
    result = []
    for coin_pair in all_futures_pairs[:20]:
        df = get_candle_data('binance', coin_pair, '1d', 300, 'Futures')
        hma = get_hull_moving_average(df, 80)

        '''
        --append pure hma of last candle
        result.append({'coin': coin_pair, 'ma_200': ma_200[1][len(df) - 1], 'hma': hma[1][len(df) - 1]})
        '''
        result.append({'coin': coin_pair, 'hma': hull_long_strategy(df, hma)})
    write_to_file([str(i) + "\n" for i in result])


@timeit
def get_avg_true_range():
    required_coins = pd.DataFrame()
    all_coin_pairs = get_all_coin_pairs()
    for coin_pair in all_coin_pairs:
        df = get_candle_data_with_timestamp('binance', coin_pair, '5m', 15, 'Futures')
        atr = get_avg_tr(df, 2)
        df2 = pd.concat([df, atr.rename('atr')], axis=1)
        df2['coin'] = coin_pair
        df2['atr_weightage'] = (df2['atr'] / df2['open']) * 100
        df2['amplitude_Hi-Op'] = (df2['high'] - df2['open']) * 100 / df2['open']
        required_coins = required_coins.append(df2[len(df) - 1:])
    required_coins.to_excel(r"H:/Trading/Scripts/trade_alert/Alerts/output.xlsx")


@timeit
def stochastic_str(*coin):
    required_coins = pd.DataFrame()
    coin_to_fetch = coin if coin else all_futures_pairs
    for coin_pair in coin_to_fetch:
        df = get_candle_data_with_timestamp('binance', coin_pair, '4h', 300, 'Futures')
        status = stochastic_crossover(coin_pair, df, '4h')
        status['coin'] = coin_pair
        rows_to_drop = []
        rows_to_include = []
        for i in range(len(status)):
            recent = datetime.strptime(status[0][i], "%Y-%m-%d %H:%M:%S") > datetime.now() - timedelta(days=2)
            if status['trend'][i] in ('bull', 'bear') and (
                    status['slowk'][i] < 10 or status['slowk'][i] > 90) and recent:
                rows_to_include.append(i)
            else:
                rows_to_drop.append(i)
        status = status.drop(rows_to_drop)
        required_coins.reset_index()
        required_coins = required_coins.append(status)
    required_coins.to_excel(r"H:/Trading/Scripts/trade_alert/Alerts/output2.xlsx")


@timeit
def get_all_patterns(*coins):
    all_coins = coins if coins else all_futures_pairs
    result = {}
    df = pd.DataFrame()
    for coin_pair in all_coins:
        df = get_candle_data_with_timestamp('binance', coin_pair, '4h', 500, 'Futures')
        df_open, high, low, close = df['open'], df['high'], df['low'], df['close']
        result = {
            ' CDL2CROWS': talib.CDL2CROWS(df_open, high, low, close),
            ' CDL3BLACKCROWS': talib.CDL3BLACKCROWS(df_open, high, low, close),
            ' CDL3INSIDE': talib.CDL3INSIDE(df_open, high, low, close),
            ' CDL3LINESTRIKE': talib.CDL3LINESTRIKE(df_open, high, low, close),
            ' CDL3OUTSIDE': talib.CDL3OUTSIDE(df_open, high, low, close),
            ' CDL3STARSINSOUTH': talib.CDL3STARSINSOUTH(df_open, high, low, close),
            ' CDL3WHITESOLDIERS': talib.CDL3WHITESOLDIERS(df_open, high, low, close),
            ' CDLABANDONEDBABY': talib.CDLABANDONEDBABY(df_open, high, low, close, penetration=0),
            ' CDLADVANCEBLOCK': talib.CDLADVANCEBLOCK(df_open, high, low, close),
            ' CDLBELTHOLD': talib.CDLBELTHOLD(df_open, high, low, close),
            ' CDLBREAKAWAY': talib.CDLBREAKAWAY(df_open, high, low, close),
            ' CDLCLOSINGMARUBOZU': talib.CDLCLOSINGMARUBOZU(df_open, high, low, close),
            ' CDLCONCEALBABYSWALL': talib.CDLCONCEALBABYSWALL(df_open, high, low, close),
            ' CDLCOUNTERATTACK': talib.CDLCOUNTERATTACK(df_open, high, low, close),
            ' CDLDARKCLOUDCOVER': talib.CDLDARKCLOUDCOVER(df_open, high, low, close, penetration=0),
            ' CDLDOJI': talib.CDLDOJI(df_open, high, low, close),
            ' CDLDOJISTAR': talib.CDLDOJISTAR(df_open, high, low, close),
            ' CDLDRAGONFLYDOJI': talib.CDLDRAGONFLYDOJI(df_open, high, low, close),
            ' CDLENGULFING': talib.CDLENGULFING(df_open, high, low, close),
            ' CDLEVENINGDOJISTAR': talib.CDLEVENINGDOJISTAR(df_open, high, low, close, penetration=0),
            ' CDLEVENINGSTAR': talib.CDLEVENINGSTAR(df_open, high, low, close, penetration=0),
            ' CDLGAPSIDESIDEWHITE': talib.CDLGAPSIDESIDEWHITE(df_open, high, low, close),
            ' CDLGRAVESTONEDOJI': talib.CDLGRAVESTONEDOJI(df_open, high, low, close),
            ' CDLHAMMER': talib.CDLHAMMER(df_open, high, low, close),
            ' CDLHANGINGMAN': talib.CDLHANGINGMAN(df_open, high, low, close),
            ' CDLHARAMI': talib.CDLHARAMI(df_open, high, low, close),
            ' CDLHARAMICROSS': talib.CDLHARAMICROSS(df_open, high, low, close),
            ' CDLHIGHWAVE': talib.CDLHIGHWAVE(df_open, high, low, close),
            ' CDLHIKKAKE': talib.CDLHIKKAKE(df_open, high, low, close),
            ' CDLHIKKAKEMOD': talib.CDLHIKKAKEMOD(df_open, high, low, close),
            ' CDLHOMINGPIGEON': talib.CDLHOMINGPIGEON(df_open, high, low, close),
            ' CDLIDENTICAL3CROWS': talib.CDLIDENTICAL3CROWS(df_open, high, low, close),
            ' CDLINNECK': talib.CDLINNECK(df_open, high, low, close),
            ' CDLINVERTEDHAMMER': talib.CDLINVERTEDHAMMER(df_open, high, low, close),
            ' CDLKICKING': talib.CDLKICKING(df_open, high, low, close),
            ' CDLKICKINGBYLENGTH': talib.CDLKICKINGBYLENGTH(df_open, high, low, close),
            ' CDLLADDERBOTTOM': talib.CDLLADDERBOTTOM(df_open, high, low, close),
            ' CDLLONGLEGGEDDOJI': talib.CDLLONGLEGGEDDOJI(df_open, high, low, close),
            ' CDLLONGLINE': talib.CDLLONGLINE(df_open, high, low, close),
            ' CDLMARUBOZU': talib.CDLMARUBOZU(df_open, high, low, close),
            ' CDLMATCHINGLOW': talib.CDLMATCHINGLOW(df_open, high, low, close),
            ' CDLMATHOLD': talib.CDLMATHOLD(df_open, high, low, close, penetration=0),
            ' CDLMORNINGDOJISTAR': talib.CDLMORNINGDOJISTAR(df_open, high, low, close, penetration=0),
            ' CDLMORNINGSTAR': talib.CDLMORNINGSTAR(df_open, high, low, close, penetration=0),
            ' CDLONNECK': talib.CDLONNECK(df_open, high, low, close),
            ' CDLPIERCING': talib.CDLPIERCING(df_open, high, low, close),
            ' CDLRICKSHAWMAN': talib.CDLRICKSHAWMAN(df_open, high, low, close),
            ' CDLRISEFALL3METHODS': talib.CDLRISEFALL3METHODS(df_open, high, low, close),
            ' CDLSEPARATINGLINES': talib.CDLSEPARATINGLINES(df_open, high, low, close),
            ' CDLSHOOTINGSTAR': talib.CDLSHOOTINGSTAR(df_open, high, low, close),
            ' CDLSHORTLINE': talib.CDLSHORTLINE(df_open, high, low, close),
            ' CDLSPINNINGTOP': talib.CDLSPINNINGTOP(df_open, high, low, close),
            ' CDLSTALLEDPATTERN': talib.CDLSTALLEDPATTERN(df_open, high, low, close),
            ' CDLSTICKSANDWICH': talib.CDLSTICKSANDWICH(df_open, high, low, close),
            ' CDLTAKURI': talib.CDLTAKURI(df_open, high, low, close),
            ' CDLTASUKIGAP': talib.CDLTASUKIGAP(df_open, high, low, close),
            ' CDLTHRUSTING': talib.CDLTHRUSTING(df_open, high, low, close),
            ' CDLTRISTAR': talib.CDLTRISTAR(df_open, high, low, close),
            ' CDLUNIQUE3RIVER': talib.CDLUNIQUE3RIVER(df_open, high, low, close),
            ' CDLUPSIDEGAP2CROWS': talib.CDLUPSIDEGAP2CROWS(df_open, high, low, close),
            ' CDLXSIDEGAP3METHODS': talib.CDLXSIDEGAP3METHODS(df_open, high, low, close),
        }
    requ_patterns = []
    for s in list(result.keys()):
        a = [{df[0][i]: result[s][i]} for i in range(len(result[s])) if result[s][i] != 0]
        if a:
            requ_patterns.append({s: a})
    print(requ_patterns)
    write_to_file([str(j) + "\n" for j in requ_patterns])


@timeit
def get_todays_range():
    required_coins = pd.DataFrame()
    all_coin_pairs = get_all_coin_pairs()
    for coin_pair in all_coin_pairs:
        df = get_candle_data_with_timestamp('binance', coin_pair, '1w', 5, 'Futures')
        df['coin'] = coin_pair
        required_coins = required_coins.append(df[len(df) - 1:])
    required_coins.to_excel(r"H:/Trading/Scripts/trade_alert/Alerts/output.xlsx")


# get_all_patterns('ADA/USDT')
# get_avg_true_range()
get_ath_diff()
# rsi_below_20()
# get_hma_crossover()
# price_below_200sma()
# get_todays_range()
