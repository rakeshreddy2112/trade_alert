import os
from concurrent.futures import ThreadPoolExecutor, as_completed

import pandas as pd

from trade_alert.Utils.decorators import timeit
from trade_alert.Utils.str_utils import volume_monitor
from trade_alert.Utils.talib_utils import get_all_coin_pairs

required_coins = pd.DataFrame()
atr_weightage_filter = 0


# market_details = get_market_details()


@timeit
def main():
    global required_coins, atr_weightage_filter
    timeframe = '1m'
    with ThreadPoolExecutor(max_workers=1) as executor:
        # future_to_f_detail  = executor.map(scalping_report,get_all_coin_pairs())
        future_to_f_detail = {executor.submit(volume_monitor, id, timeframe): (id, timeframe) for id in
                              get_all_coin_pairs()}
        # executor.shutdown(wait=True )
        for future in as_completed(future_to_f_detail):
            required_coins = required_coins.append(future.result())
        # required_coins = required_coins.sort_values('atr_weightage', ascending=False)
        # filtered = required_coins[required_coins['atr_weightage'] >= atr_weightage_filter]
        if required_coins.empty:
            print("no coins available")
        else:
            try:
                required_coins.to_excel(os.getcwd() + "//scalping.xlsx", float_format="%.2f")
            except:
                required_coins.to_excel(os.getcwd() + "//scalping2.xlsx", float_format="%.2f")
                print("written to scalping2 file")


if __name__ == '__main__':
    main()
