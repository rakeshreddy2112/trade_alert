from datetime import datetime

import ccxt
import numpy as np
import pandas as pd
import talib
import math

all_futures_pairs = (
    '1INCH/USDT', 'AAVE/USDT', 'ADA/USDT', 'AKRO/USDT', 'ALGO/USDT', 'ALICE/USDT', 'ALPHA/USDT',
    'ANKR/USDT', 'ATOM/USDT', 'AVAX/USDT', 'AXS/USDT', 'BAKE/USDT', 'BAL/USDT', 'BAND/USDT', 'BAT/USDT', 'BCH/USDT',
    'BEL/USDT', 'BLZ/USDT', 'BNB/USDT', 'BTS/USDT', 'BTT/USDT', 'BZRX/USDT', 'CELR/USDT', 'CHR/USDT',
    'CHZ/USDT', 'COMP/USDT', 'COTI/USDT', 'CRV/USDT', 'CTK/USDT', 'CVC/USDT', 'DASH/USDT', 'DENT/USDT',
    'DGB/USDT', 'DODO/USDT', 'DOGE/USDT', 'DOT/USDT', 'EGLD/USDT', 'ENJ/USDT', 'EOS/USDT', 'ETC/USDT', 'ETH/USDT',
    'FIL/USDT', 'FLM/USDT', 'GRT/USDT', 'GTC/USDT', 'HBAR/USDT', 'HNT/USDT', 'HOT/USDT', 'ICP/USDT', 'ICX/USDT',
    'IOST/USDT', 'IOTA/USDT', 'KAVA/USDT', 'KEEP/USDT', 'KNC/USDT', 'KSM/USDT', 'LINA/USDT', 'LINK/USDT',
    'LIT/USDT',
    'LRC/USDT', 'LTC/USDT', 'LUNA/USDT', 'MANA/USDT', 'MATIC/USDT', 'MKR/USDT', 'MTL/USDT', 'NEAR/USDT', 'NEO/USDT',
    'NKN/USDT', 'OCEAN/USDT', 'OGN/USDT', 'OMG/USDT', 'ONE/USDT', 'ONT/USDT', 'QTUM/USDT', 'REEF/USDT', 'REN/USDT',
    'RLC/USDT', 'RSR/USDT', 'RUNE/USDT', 'RVN/USDT', 'SAND/USDT', 'SC/USDT', 'SFP/USDT', 'SKL/USDT', 'SNX/USDT',
    'SOL/USDT', 'SRM/USDT', 'STMX/USDT', 'STORJ/USDT', 'SUSHI/USDT', 'THETA/USDT', 'TLM/USDT', 'TOMO/USDT',
    'TRX/USDT',
    'UNFI/USDT', 'UNI/USDT', 'VET/USDT', 'WAVES/USDT', 'XEM/USDT', 'XLM/USDT', 'XMR/USDT', 'XRP/USDT', 'XTZ/USDT',
    'YFII/USDT', 'YFI/USDT', 'ZEC/USDT', 'ZEN/USDT', 'ZIL/USDT', 'ZRX/USDT')

interested_coins = ('BTC/USDT', 'WAVES/USDT', 'CHZ/USDT', 'ALGO/USDT', 'HBAR/USDT', 'SOL/USDT', 'XRP/USDT')


def convert_number(value):
    return float(str(float(np.format_float_scientific(value, unique=False, precision=8))).split('e-')[0])


def get_candle_data(exchange, symbol, interval, limit, derivative):
    try:
        if derivative:
            exchange = ccxt.binance({'enableRateLimit': True, 'options': {'defaultType': 'future', }})
        else:
            exchange = ccxt.binance({'enableRateLimit': True})
        kline = exchange.fetch_ohlcv(symbol, interval, limit=int(limit))
        df = pd.DataFrame(np.array(kline), columns=['open_time', 'open', 'high', 'low', 'close', 'volume'],
                          dtype='float64')
        """
        op = df['open']
        hi = df['high']
        lo = df['low']
        cl = df['close']
        vl = df['volume']
        timestamps = df['open_time']
        """
        return df
    except Exception as e:
        print(e)


def get_candle_timestamp(df):
    timestamps = df['open_time']
    ts = [datetime.fromtimestamp(int(i / 1000)).strftime('%Y-%m-%d %H:%M:%S') for i in timestamps]
    return ts


def get_ma(candle_data_df, time_period):
    cl = candle_data_df['close']
    ma = talib.MA(cl, timeperiod=time_period, matype=0)
    ts = get_candle_timestamp(candle_data_df)
    ma_with_time = pd.concat([pd.Series(ts), ma], axis=1)
    return ma_with_time


def get_basic_indicators(ind_type, candle_data_df, time_period):
    talib_api = {'EMA': talib.EMA, 'RSI': talib.RSI, 'WMA': talib.WMA}
    cl = candle_data_df['close']
    ind_vals = talib_api.get(ind_type.upper())(cl, timeperiod=time_period)
    ts = get_candle_timestamp(candle_data_df)
    return pd.concat([pd.Series(ts), ind_vals], axis=1)


def get_candle_data_with_timestamp(exchange, symbol, interval, limit, derivative):
    df = get_candle_data(exchange, symbol, interval, limit, derivative)
    ts = get_candle_timestamp(df)
    new_df = pd.concat([pd.Series(ts).rename('open_timestamp'), df], axis=1)
    return new_df


def write_to_file(data):
    file_object = open(r"sample.txt", "w")
    file_object.writelines(data)
    file_object.close()


def get_hull_moving_average(df, time_period):
    cl = df['close']
    wma1 = talib.WMA(cl, time_period)
    wma2 = talib.WMA(cl, time_period / 2)
    sqrt = math.sqrt(time_period)
    wma3 = 2 * wma2 - wma1
    hull_ma = talib.WMA(wma3, sqrt)
    ts = get_candle_timestamp(df)
    return pd.concat([pd.Series(ts), hull_ma], axis=1)


def get_avg_tr(df, time_period):
    real = talib.ATR(df['high'], df['low'], df['close'], timeperiod=time_period)
    return real


def get_stochastic(hi, lo, cl, fastk_period=14, slowk_period=3, slowk_matype=0, slowd_period=3,
                   slowd_matype=0):
    slowk, slowd = talib.STOCH(hi, lo, cl, fastk_period=14, slowk_period=3, slowk_matype=0, slowd_period=3,
                               slowd_matype=0)
    return slowk, slowd


def get_all_coin_pairs():
    exchange = ccxt.binance({'enableRateLimit': True, 'options': {'defaultType': 'future', }})
    load_markets = exchange.load_markets()
    all_coin_pairs = list(load_markets.keys())
    return all_coin_pairs
