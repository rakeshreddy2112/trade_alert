from trade_alert.Utils.talib_utils import get_candle_data, get_basic_indicators, get_candle_timestamp, \
    get_candle_data_with_timestamp, get_stochastic, get_ma
import pandas as pd


def rsi_identifier(coin, time_period):
    rsi_period = 5
    no_of_candles_to_fetch = 18
    df = get_candle_data('binance', coin, time_period, no_of_candles_to_fetch, None)
    a = get_basic_indicators('RSI', df, rsi_period)
    crt_rsi = round(a.iloc[no_of_candles_to_fetch - 1][1], 1)
    return crt_rsi


def volume_monitor(coin, time_period):
    result = []
    no_of_candles_to_fetch = 10 * 96
    df = get_candle_data('binance', coin, time_period, no_of_candles_to_fetch, 'Futures')
    ts = get_candle_timestamp(df)
    volume = df['volume']
    new_df = pd.concat([pd.Series(ts), volume], axis=1)
    n_times = 0
    for i in range(1, len(new_df)):
        if new_df['volume'][i] > 6 * new_df['volume'][i - 1]:
            n_times = round(new_df['volume'][i] / new_df['volume'][i - 1])
            price_change = ((df['high'][i] - df['high'][i - 1]) * 100) / df['high'][i - 1]
            result.append({'time': new_df[0][i], 'volume': new_df['volume'][i], 'n-times': n_times,
                           'price_change': round(price_change, 2)})
    if result:
        print(coin, sorted(result, key=lambda x: x['time'], reverse=True), n_times, " times")
    return sorted(result, key=lambda x: x['time'], reverse=True)


def growth_monitor(coin_pair):
    no_of_candles_to_fetch = 7
    df = get_candle_data_with_timestamp('binance', coin_pair, '1M', no_of_candles_to_fetch, 'Futures')
    old_open = df['open'][0]
    current_high = df['high'][4]
    change_in_price = ((current_high - old_open) * 100) / old_open
    return round(change_in_price, 2)


def ath_diff(coin_pair):
    no_of_candles_to_fetch = 7
    df = get_candle_data_with_timestamp('binance', coin_pair, '1M', no_of_candles_to_fetch, 'Futures')
    try:
        ath_2021 = max([df['high'][i] for i in range(len(df) - 2)]) if len(df) > 2 else 1010
        atl_2021 = min([df['low'][i] for i in range(len(df) - 4, len(df) - 2)]) if len(df) > 4 else 1010
        current_high = df['high'][len(df) - 1]
        difference_from_current_low = ((current_high - atl_2021) * 100) / atl_2021
        if ath_2021 >= current_high:
            diff_to_previous_high = ((ath_2021 - current_high) * 100) / current_high
            trend = 'yet to reach'
        else:
            diff_to_previous_high = ((current_high - ath_2021) * 100) / ath_2021
            trend = 'crossed previous 2021-high'
        return {'diff_to_previous_high': diff_to_previous_high, 'trend': trend, 'ath_2021': ath_2021, 'atl': atl_2021,
                'difference_from_current_low': difference_from_current_low}
    except Exception as e:
        print(e, coin_pair)
        return None


def stochastic_crossover(coin, df, time_period):
    op = df['open']
    hi = df['high']
    lo = df['low']
    cl = df['close']
    vl = df['volume']
    timestamps = df['open_time']

    slowk, slowd = get_stochastic(hi, lo, cl, fastk_period=14, slowk_period=3, slowk_matype=0, slowd_period=3,
                                  slowd_matype=0)
    stch_mntm = slowk - slowd
    diff = pd.Series(['positive' if i >= 0 else 'Negative' for i in stch_mntm])
    ma_200 = get_ma(df, 200)
    stch_vs_ma_200 = pd.Series(['bull' if df['close'][i] >= ma_200[1][i] else 'bear' for i in range(len(df))])
    df2 = pd.concat([df, pd.Series(stch_vs_ma_200).rename('trend'), diff.rename('momentum'), slowk.rename('slowk'),
                     slowd.rename('slowd')], axis=1)
    cross_over = ['crossover' if df2['momentum'][i - 1] != df2['momentum'][i] else 'None' for i in range(1, len(df))]
    cross_over.insert(0, 'start')
    final_df = pd.concat([df2, pd.Series(cross_over).rename('crossover'), pd.Series(ma_200[1]).rename('ma_200')],
                         axis=1)
    return final_df
