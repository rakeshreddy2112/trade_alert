import pandas as pd
import os

import requests

from trade_alert.Utils.talib_utils import get_all_coin_pairs, get_candle_data_with_timestamp, get_avg_tr, \
    get_basic_indicators
from trade_alert.Utils.decorators import timeit
from concurrent.futures import ThreadPoolExecutor, as_completed
from trade_alert.Utils.coingecko import get_market_details
from trade_alert.Utils.ecosystem import eco_systems

required_coins = pd.DataFrame()
atr_weightage_filter = 0
market_details = get_market_details()


@timeit
def scalping_report(*coin):
    global required_coins
    coins_to_fetch = coin if coin else get_all_coin_pairs()
    rsi_period = 5
    for coin_pair in coins_to_fetch:
        df = get_candle_data_with_timestamp('binance', coin_pair, '1M', 15, 'Futures')
        atr = get_avg_tr(df, 2)
        rsi = get_basic_indicators('RSI', df, rsi_period)
        df2 = pd.concat([df, atr.rename('atr')], axis=1)
        df2['rsi-' + str(rsi_period)] = rsi[1]
        df2['coin'] = coin_pair
        df2['atr_weightage'] = (df2['atr'] / df2['open']) * 100
        df2['current_amplitude%(Hi-Op)'] = (df2['high'] - df2['open']) * 100 / df2['open']
        df2['prev-1_amplitude%'] = df2['current_amplitude%(Hi-Op)'].shift(periods=1)
        df2['prev-2_amplitude%'] = df2['current_amplitude%(Hi-Op)'].shift(periods=2)
        df2['prev-3_amplitude%'] = df2['current_amplitude%(Hi-Op)'].shift(periods=3)
        df2['market_cap_rank'] = market_details.get(coin_pair.rstrip('/USDT').rstrip('/BUSD'), {}).get(
            'market_cap_rank')
        df2['market_cap'] = market_details.get(coin_pair.rstrip('/USDT').rstrip('/BUSD'), {}).get('market_cap')
        df2['eco_systems'] = str(eco_systems.get(coin_pair.rstrip('/USDT').rstrip('/BUSD'), 'NA'))
        return df2[len(df) - 1:]


@timeit
def main():
    global required_coins, atr_weightage_filter
    with ThreadPoolExecutor(max_workers=60) as executor:
        # future_to_f_detail  = executor.map(scalping_report,get_all_coin_pairs())
        future_to_f_detail = {executor.submit(scalping_report, id): id for id in get_all_coin_pairs()}
        # executor.shutdown(wait=True )
        for future in as_completed(future_to_f_detail):
            required_coins = required_coins.append(future.result())
        required_coins = required_coins.sort_values('atr_weightage', ascending=False)
        filtered = required_coins[required_coins['atr_weightage'] >= atr_weightage_filter]
        if filtered.empty:
            print("no coins available")
        else:
            try:
                filtered.to_excel(os.getcwd() + "//scalping.xlsx", float_format="%.2f")
            except:
                filtered.to_excel(os.getcwd() + "//scalping2.xlsx", float_format="%.2f")
                print("written to scalping2 file")


main()
"""
#Observations:
1w- timeframe:
if weekly price change <1:
    price change is positive in next one-two weeks
    
check avg atr/price change in last 5 prev times - to check if the growth or drop is constant and not fake breakout

weekly stochastic below 30
"""
