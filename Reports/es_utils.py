from datetime import datetime
import os
from dateparser import parse
from elasticsearch import Elasticsearch
import pytz

from trade_alert.Utils.decorators import timeit

es = Elasticsearch(['http://localhost', ], http_auth=('elastic', os.environ.get(
    'elastic_pass')), scheme="http", port=9200, timeout=30, max_retries=10, retry_on_timeout=True)


def index_latest_atr_data(required_coins, timeframe):
    timeframe = update_time_frame(timeframe)
    try:
        t = required_coins.T
        t.columns = t.loc['coin']
        row_headers = list(t.columns.values)
        column_headers = list(required_coins.columns.values)
        for i in row_headers:
            data_body = {}
            for col in range(len(column_headers)):
                if column_headers[col] == 0:
                    data_body[str(column_headers[col])] = parse(t[i][column_headers[col]])
                elif column_headers[col] == 'open_time':
                    data_body[str(column_headers[col])] = datetime.fromtimestamp(int(t[i][column_headers[col]]) / 1000)
                else:
                    data_body[str(column_headers[col])] = t[i][column_headers[col]]
            result = es.index(index='atr_weightage_' + timeframe, doc_type='_doc', id=i, body=data_body)
        print("indexed to", 'atr_weightage_' + timeframe)
    except Exception as e:
        print(result)
        print("data", data_body)


def update_time_frame(timeframe):
    """ correction for index id :!caps not allowed"""
    if timeframe == '1m':
        timeframe = '1minute'
    elif timeframe == '1M':
        timeframe = '1month'
    return timeframe


# @timeit
def index_df(required_coins, timeframe):
    timeframe = update_time_frame(timeframe)
    length_of_df = len(required_coins)
    for i in range(length_of_df):
        process_df_rows(required_coins.iloc[i], timeframe)


def process_df_rows(df_row, timeframe):
    try:
        index_unique_id = df_row['coin'] + relative_time(df_row['open_timestamp'], timeframe)
        data_body = {i: j for i, j in df_row.items()}
        data_body['open_timestamp_str'] = data_body['open_timestamp']
        timezone = pytz.timezone('Asia/Kolkata')
        data_body['open_timestamp'] = timezone.localize(
            datetime.strptime(data_body['open_timestamp'], '%Y-%m-%d %H:%M:%S'))
        result = es.index(index='atr_weightage_' + timeframe, doc_type='_doc', id=index_unique_id, body=data_body)
    except Exception as e:
        print(e)


def relative_time(open_timestamp, timeframe):
    time_sensitivity = timeframe[::-1][0]
    time_stamp_for_index = {'m': 16, 'e': 16, 'h': 13, 'd': 10, 'w': 10, 'M': 7}
    return open_timestamp[:time_stamp_for_index.get(time_sensitivity)]
