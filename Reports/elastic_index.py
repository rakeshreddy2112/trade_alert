from concurrent.futures import ThreadPoolExecutor, as_completed

import pandas as pd

from es_utils import index_df
from trade_alert.Utils.coingecko import get_market_details
from trade_alert.Utils.decorators import timeit
from trade_alert.Utils.talib_utils import get_all_coin_pairs, get_candle_data_with_timestamp, get_avg_tr, \
    get_basic_indicators

index_data = {}
body = []

atr_weightage_filter = 0
market_details = get_market_details()


def scalping_report(coin_pair, time_frame):
    rsi_period = 5
    df = get_candle_data_with_timestamp('binance', coin_pair, time_frame, 15, 'Futures')
    atr = get_avg_tr(df, 2)
    rsi = get_basic_indicators('RSI', df, rsi_period)
    df2 = pd.concat([df, atr.rename('atr')], axis=1)
    df2['rsi-' + str(rsi_period)] = rsi[1]
    df2['coin'] = coin_pair
    df2['atr_weightage'] = (df2['atr'] / df2['open']) * 100
    df2['current_amplitude%(Hi-Op)'] = (df2['high'] - df2['open']) * 100 / df2['open']
    df2['prev-1_amplitude%'] = df2['current_amplitude%(Hi-Op)'].shift(periods=1)
    df2['prev-2_amplitude%'] = df2['current_amplitude%(Hi-Op)'].shift(periods=2)
    df2['prev-3_amplitude%'] = df2['current_amplitude%(Hi-Op)'].shift(periods=3)
    df2['market_cap_rank'] = market_details.get(coin_pair.rstrip('/USDT').rstrip('/BUSD'), {}).get(
        'market_cap_rank')
    df2['market_cap'] = market_details.get(coin_pair.rstrip('/USDT').rstrip('/BUSD'), {}).get('market_cap')
    df2['vol_change'] = round(df2['volume'].shift(periods=1) / df2['volume'], 2)
    df2['prev_volume_change'] = df2['vol_change'].shift(periods=1)
    temp_df = pd.DataFrame()
    index_df(temp_df.append(df2[len(df) - 5:]), time_frame)
    return df2[len(df) - 5:]


@timeit
def main(timeframe):
    global required_coins, atr_weightage_filter
    required_coins = pd.DataFrame()
    with ThreadPoolExecutor(max_workers=50) as executor:
        future_to_f_detail = {executor.submit(scalping_report, id, timeframe): (id, timeframe) for id in
                              get_all_coin_pairs()}
        for future in as_completed(future_to_f_detail):
            required_coins = required_coins.append(future.result())
        return required_coins, timeframe


short_time_frames = ('5m',)  # '5m', '15m','1h')
long_time_frames = ('1d', '1w', '1M')
for timeframe in short_time_frames:
    required_coins, time_frame = main(timeframe)
    print("indexing finished for timeframe", timeframe)
