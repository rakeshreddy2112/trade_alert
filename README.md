TALib Set up-:
https://www.lfd.uci.edu/~gohlke/pythonlibs/#ta-lib
--Download .whl and use command "pip install TALIB_FILE_NAME.whl"
GIT Readme.md for instructins:
https://github.com/mrjbq7/ta-lib

MAC:
Rosetta2 emulator for the new ARM silicon (M1 chip). 
Installed Rosetta2 via terminal using:
/usr/sbin/softwareupdate --install-rosetta --agree-to-license
This will install rosetta2 with no extra button clicks.
After installing Rosetta2 above ,open terminal through getinfo-open as rosette terminal- quit and open again)
install homebrew in rosette terminal
arch -x86_64 /bin/bash -c "$(curl -fsSL https:/raw.githubusercontent.com/Homebrew/install/master/install.sh)"
Once Homebrew for M1 ARM is installed use this Homebrew command to install packages:

$ arch -x86_64 brew install python@3.9
$ arch -x86_64 brew install ta-libs
$ arch -x86_64 brew link ta-lib
$ arch -x86_64 python3.9 -m pip install --no-cache-dir ta-lib